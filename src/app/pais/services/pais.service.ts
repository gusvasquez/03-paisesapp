import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../interfaces/pais.interface';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private apiUrl: string = 'https://restcountries.com/v3.1';


  constructor(private _http: HttpClient) { }

  buscarPais(termino: string): Observable<Country[]> {
    const url = `${this.apiUrl}/name/${termino}`
    return this._http.get<Country[]>(url);
  }

  buscarCapital(termino: string): Observable<Country[]> {
    const url = `${this.apiUrl}/capital/${termino}`;
    return this._http.get<Country[]>(url);
  }
  // no se ponen los corchetes [] porque se va a retornar un solo pais
  getPaisId(id: string): Observable<Country> {
    const url = `${this.apiUrl}/alpha/${id}`;
    return this._http.get<Country>(url);
  }

  getRegion(region: string): Observable<Country[]>{
    const url = `${this.apiUrl}/region/${region}`;
    return this._http.get<Country[]>(url);
  }

}
