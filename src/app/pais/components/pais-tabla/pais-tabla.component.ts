import { Component, Input, OnInit } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-pais-tabla',
  templateUrl: './pais-tabla.component.html'
})
export class PaisTablaComponent implements OnInit {

  // decorador input nos sirve para pasarle datos de un componente patre a un componente hijo en este caso le pasamos paises
  @Input() paises: Country[] =[];

  constructor() { }

  ngOnInit(): void {
  }

}
