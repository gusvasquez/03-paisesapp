import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs';

@Component({
  selector: 'app-pais-input',
  templateUrl: './pais-input.component.html'
})
export class PaisInputComponent implements OnInit {


  public termino: string = '';
  // Output emitir un metodo a un componente padre
  @Output() onEnter: EventEmitter<string> = new EventEmitter();
  @Output() onDebounce: EventEmitter<string> = new EventEmitter();

  // recibe el input que le enviamos de los componentes donde estemos recibe el string que le enviamos
  @Input() placeholder:string ='';

  public debouncer: Subject<string> = new Subject();

  buscar() {
    this.onEnter.emit(this.termino);
  }

  ngOnInit() {
    this.debouncer
    .pipe(debounceTime(300)) // el pipe con el debounceTime para que deja de emitir valores en los porximos 300 mlsegundos
    .subscribe(valor => {
      // se emite el valor
      this.onDebounce.emit(valor);
    });
  }

  teclaPrecionada(){
    // le enviamos el siguiente valor con ext
    this.debouncer.next(this.termino);
  }


}
