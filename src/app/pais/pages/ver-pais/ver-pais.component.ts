import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaisService } from '../../services/pais.service';
import { switchMap, tap } from 'rxjs';
import { Country } from '../../interfaces/pais.interface';

@Component({
  selector: 'app-ver-pais',
  templateUrl: './ver-pais.component.html'
})
export class VerPaisComponent implements OnInit {

  // con el ! le decimos que puede ser null o no, para que confie en lo que le pasemos
  pais!: Country;

  constructor(private activateRoute: ActivatedRoute, private _paisService: PaisService) { }

  ngOnInit(): void {
    // otro metodod utilizando switchMap que recibir el oserbable anterior y retorna un obserbable nuevo
    this.activateRoute.params
       // el tap recibe el producto del observable del pipe y imprime lo que responda
      .pipe(switchMap(({ id }) => this._paisService.getPaisId(id)), tap(console.log))
      .subscribe(
        response => {
          // [0] se pone en  la primera posicion porque es un arreglo y dentro esta el objeto
          this.pais = response[0];
        }
      );
    //this.activateRoute.params.subscribe(
    // en ves de poner params.id sustraemos con los corchetes el id que nos llega por la url como parametro
    //  ({ id }) => {
    //    console.log(id);
    //    this._paisService.getPaisId(id).subscribe(
    //      response => {
    //        console.log(response);
    //      }, error => {
    //        console.log(error);
    //      }
    //    );
    //  }
    //);
  }

}
