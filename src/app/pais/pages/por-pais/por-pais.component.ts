import { Component } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-pais',
  templateUrl: './por-pais.component.html',
  styles: [
    `
    li{
      cursor: pointer;
    }
    `
  ]
})
export class PorPaisComponent  {

  public termino: string ='';
  public hayError: boolean = false;
  public paises: Country[] = [];
  public paisesSugeridos: Country[] = [];
  public mostrarSugerencias: boolean = false;

  constructor(private _paisService: PaisService) { }

  buscar(termino:string){
    this.mostrarSugerencias = false;
    this.hayError = false;
    this.termino = termino;
    this._paisService.buscarPais(this.termino).subscribe(
      response =>{
        // se llena el arreglo de tipo Country con las respuesta que nos llega
        this.paises = response;
        console.log(this.paises);
      }, err => {
        this.hayError= true;
        // si no regresan datos se pone el arreglo vacio
        this.paises = [];
      }
    );
  }

  sugerencias(event: string){
    this.hayError = false;
    this.mostrarSugerencias= true;
    // TODO: crear sugerencias
    this._paisService.buscarPais(event).subscribe(
      response =>{
        // ponemos el splice para no mas mostrar 5 datos del arreglo que se consulta
        this.paisesSugeridos = response.splice(0,5);
      }
    );
  }


}
