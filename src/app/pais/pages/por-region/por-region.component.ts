import { Component } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-region',
  templateUrl: './por-region.component.html',
  styles: [
    `
    button{
      margin-right: 5px;
    }
    `
  ]
})
export class PorRegionComponent {

  public regiones: string[] = ['africa', 'america', 'asia', 'europe', 'oceania'];
  public regionActiva: string = '';
  public paises: Country[]=[];

  constructor(private _paisService: PaisService) { }

  getRegion(region: string): string {
    return (region === this.regionActiva) ? 'btn btn-primary' : 'btn btn-outline-primary';
  }

  activarRegion(region: string) {
    this.regionActiva = region;
    console.log(this.regionActiva);
    // llamamos al metodo al servicio
    this._paisService.getRegion(this.regionActiva).subscribe(
      response => {
        this.paises = response;
      }, error => {
        this.paises = [];
      }
    );
  }

}
