import { Component, OnInit } from '@angular/core';
import { Country } from '../../interfaces/pais.interface';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-por-capital',
  templateUrl: './por-capital.component.html'
})
export class PorCapitalComponent {

  public termino: string = '';
  public hayError: boolean = false;
  public paises: Country[] = [];

  constructor(private _paisService: PaisService) { }

  buscar(termino: string) {
    this.hayError = false;
    this.termino = termino;
    this._paisService.buscarCapital(this.termino).subscribe(
      response => {
        this.paises = response;
        console.log(this.paises);
      }, error => {
        this.hayError = true;
        this.paises= [];
      }
    );
  }
}
